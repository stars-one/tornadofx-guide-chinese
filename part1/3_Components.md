<meta name="referrer" content="no-referrer">

# 组件

JavaFX使用戏剧类比来组织带有`Stage(舞台)`和`Scene(场景)`组件的“应用程序”。 TornadoFX通过提供`View`，`Controller`和`Fragment`组件在此基础上构建。 TornadoFX使用`Stage`和`Scene`时，`View`，`Controller`和`Fragment`引入了简化开发的新概念。 其中许多组件都为单例且是自动维护的，并且可以通过TornadoFX的简单依赖项注入或直接实例化从而进行数据的相互传递。 

您还可以选择使用FXML(其属于JavaFx中的知识)，关于此方面的详见第10章。

现在首先，让我们扩展App来创建一个启动TornadoFX应用程序的入口点。

## 应用APP和视图基础

你必须要有一个类继承了**APP**这个类，才可以创建一个TornadoFx的应用程序。

App类是应用程序的入口点，并指定了初始View。 实际上它继承了JavaFX的Application ，但是您不一定需要指定一个start()或main()方法。

但首先，让我们继承App来创建自己的实现，并将主视图（primary view）指定为构造函数的第一个参数。

```kotlin
import tornadofx.*

class MyApp: App(MyView::class)
```

一个视图（View）包含显示逻辑以及节点（Nodes）的布局，类似于JavaFX的Stage。 它被作为单例（singleton）来自动管理。 当您声明一个View，您必须指定一个root属性，该属性可以是任何Node类型，并且将保存视图（View）的内容。

在同一个Kotlin文件或在另一个新文件中，你可以声明一个新类去继承View这个类，之后，给`root`赋值为`VBox`(或者是其他的Node)。

```kotlin
import tornadofx.*

class MyView: View() {
    override val root = vbox {
    }
}
```

但是，我们需要往这个作为根布局的VBox添加控件,我们可以使用初始化程序块(initializer block)，让我们添加一个JavaFX的`Button`和一个`Label`。 您可以使用 “plus assign” `+=`运算符将子项添加到任何Pane类型，包括这里的VBox。

还要注意`tornadofx.*`的导入。 这很重要，并且应该出现在所有与TornadoFX相关的文件中。 之所以如此重要，是因为如果没有导入，IDE无法发现该框架的某些功能。 此导入启用了一些您实际上需要的高级扩展功能。

```kotlin
import tornadofx.*

class MyView: View() {
    override val root = vbox {
        button("Press me")
        label("Waiting")
    }
}
```

TornadoFX提供了一个可以简化UI代码的构建器语法。与创建UI元素并手动将它们添加到父元素的子列表不同，构建器允许我们将UI表示为层次结构，从而使您能够非常容易地可视化生成的UI。注意，**所有构建器都是用小写字母编写的**，这样就不会与UI元素类的手动实例化相混淆。

## 启动一个TornadoFX应用

新版本的JVM知道如何不通过一个`main()`方法去启动一个JavaFx应用程序。JavaFx应用和TornadoFx应用都是继承于`javafx.application.Application`这个类。`tornadofx.App`也是继承于`javafx.application.Application`，与JavaFx没有不同。因此你可以通过引用`com.example.app.MyApp`这个类来启动应用而不需要通过`main()`方法(除非您需要提供命令行参数)。在这种情况下，您将需要添加一个包级别的主函数到`MyApp.kt`文件。

```kotlin
fun main(args: Array<String>) {
  launch<MyApp>(args)
}
```

个主函数将被编译进`com.example.app.MyAppKt`，注意最后的`Kt`。当您创建包级别的主函数时，它将始终具有完全限定包的类名，加上文件名，加上`Kt`。

我们将使用Intellij IDEA去启动和调试`App`,导航到*Run*→*Edit Configurations* （图3.1）

**图3.1**

![](https://upload-images.jianshu.io/upload_images/3764796-d06beccfcc313832.png?imageMogr2/auto-orient/strip|imageView2/2/w/736/format/webp)

点击绿色的"+"号去创建一个新的应用程序配置(图3.2)

**图3.2**

![](https://upload-images.jianshu.io/upload_images/3764796-d5f594308cd3c240.png?imageMogr2/auto-orient/strip|imageView2/2/w/622/format/webp)

指定 “主类（Main class）” 的名称，这应该是您的App类。 您还需要指定它所在的模块（module）。给配置一个有意义的名称，如 “Launcher”。 之后点击 “OK”（图3.3）。

**图3.3**

![](https://upload-images.jianshu.io/upload_images/3764796-049157e10411fbb3.png?imageMogr2/auto-orient/strip|imageView2/2/w/787/format/webp)

您可以通过选择*Run*→*Run 'Launcher'*或任何您命名的配置来运行 TornadoFX应用程序（图3.4）。

**图3.4**

![](https://upload-images.jianshu.io/upload_images/3764796-0a119672eb5417c0.png?imageMogr2/auto-orient/strip|imageView2/2/w/806/format/webp)

你现在应该可以看到你的应用成功启动了(图3.5)

**图3.5**

![](https://upload-images.jianshu.io/upload_images/3764796-cf3c55843e8625dc.png?imageMogr2/auto-orient/strip|imageView2/2/w/122/format/webp)

恭喜你！你编写了你第一个TornadoFx应用(虽然简单)。它现在看起来可能不是很好，但是当我们涵盖更多TornadoFX的强大功能时，我们将创建大量令人印象深刻的用户界面，几乎没有多少代码，而且只需要很少时间。 但首先让我们来更好地了解App和View之间发生的情况。

## 认识视图(View)

让我们更深入地了解“View”是如何工作以及我们应该如何调用它。 一个View包含一个从根开始的JavaFX节点(Node)层次。

在下一节中，我们将学习如何利用构建器来快速创建这些节点层次结构。 

TornadoFX只维护一个`MyView`实例，可以保证其是单例。 TornadoFX还支持范围(Scope)，在定义的范围中可以将`View`，片段(`Fragment`)和控制器(`Controller`)的集合组合在一起,这样就会让View成为该范围中的一个单例。 这对于[多文档界面应用程序](https://msdn.microsoft.com/zh-cn/library/aa263481.aspx)和其他高级使用情况十分有用。

## 注入视图

你可以在在其他的View中注入一个或多个View，下面我们将`TopView`和`BottomView`注入到`MasterView`中,我们将子View(`TopView`和`BottomView`)分配给了`BorderPane`,效果如图3.6所示

```kotlin
import javafx.scene.control.Label
import javafx.scene.layout.BorderPane
import tornadofx.*


class MasterView: View() {
    override val root = borderpane {
        top<TopView>()
        bottom<BottomView>()
    }
}

class TopView: View() {
    override val root = label("Top View")
}

class BottomView: View() {
    override val root = label("Bottom View")
}
```

![图3.6](https://upload-images.jianshu.io/upload_images/3764796-1bd743a9c7309fa0.png?imageMogr2/auto-orient/strip|imageView2/2/w/178/format/webp)


## 注入视图(使用find方法或inject方法)

`inject()`方法
The `inject()` delegate will lazily assign a given component to a property. The first time that component is called is when it will be retrieved.
Alternatively, instead of using the `inject()` delegate you can use the `find()` function to retrieve a singleton instance of a `View` or other components. In the following example
we retrieve an instance of the `TopView` and `BottomView` using `inject()` and `find()` respectively, and assign the root element of those sub views to the sections inside the `BorderPane`.
This is normally not necessary but might help you understand how the above short hand works under the covers.

```kotlin
import javafx.scene.control.Label
import javafx.scene.layout.BorderPane
import tornadofx.*


class MasterView : View() {
    // Explicitly retrieve TopView
    val topView = find(TopView::class)
    // Create a lazy reference to BottomView
    val bottomView: BottomView by inject()

    override val root = borderpane {
        top = topView.root
        bottom = bottomView.root
    }
}
```
You can use either `find()` or `inject()`, but using `inject()` delegates is the most idiomatic means to perform dependency injection and has the advantage of lazy loading.


## Controllers

In many cases, it is considered a good practice to separate a UI into three distinct parts:

1. **Model** - The business code layer that holds core logic and data
2.  **View** - The visual display with various input and output controls
3.  **Controller** - The "middleman" mediating events between the Model and the View

> There are other flavors of MVC like MVVM and MVP, all of which can be leveraged in TornadoFX.

While you could put all logic from the Model and Controller right into the view, it is often cleaner to separate these three pieces distinctly to maximize reusability. One commonly used pattern to accomplish this is the MVC pattern. In TornadoFX, a `Controller` can be injected to support a `View`.

Here is a simple example. Create a simple `View` with a `TextField` whose value is bound to an observable string property and later written to a "database" when a `Button` is clicked. We can inject a `Controller` that handles interacting with the model that writes to the database. Since this example is simplified, there will be no database but a printed message will serve as a placeholder (Figure 3.7).


```kotlin
import tornadofx.*


class MyView : View() {
    val controller: MyController by inject()
    val input = SimpleStringProperty()

    override val root = form {
        fieldset {
            field("Input") {
                textfield(input)
            }

            button("Commit") {
                action {
                    controller.writeToDb(input.value)
                    input.value = ""
                }
            }
       }
    }
}


class MyController: Controller() {
    fun writeToDb(inputValue: String) {
        println("Writing $inputValue to database!")
    }
}
```

Notice how the `input` property is automatically bound to the textfield just by referencing it in the `textfield` builder. As an alternative, you could have created a
reference to the textfield and retrieved its `text` property, but that approach would create more complex UI code without much benefit. In rare cases you might
need to reference individual UI elements.

**Figure 3.7**

![](https://i.imgur.com/CKG7Fb9.png)

When we build the UI, we make sure to add a reference to the `inputField` so that it can be referenced from the `onClick`
event handler of the "Commit" button later, hence why we save it to a variable. When the "Commit" button is clicked, you will see the Controller prints  a line to the console.

```
Writing Alpha to database!
```

It is important to note that while the above code works, and may even look pretty good, it is a good practice to avoid
referencing other UI elements directly. Your code will be much easier to refactor if you bind your UI elements to
properties and manipulate the properties instead. We will introduce the `ViewModel` later, which provides even easier
ways to deal with this type of interaction.

You can also use Controllers to provide data to a `View` (Figure 3.8).

```kotlin
import javafx.collections.FXCollections
import tornadofx.*


class MyView : View() {
    val controller: MyController by inject()

    override val root = vbox {
        label("My items")
        listview(controller.values)
    }
}

class MyController: Controller() {
    val values = FXCollections.observableArrayList("Alpha","Beta","Gamma","Delta")
}
```
**Figure 3.8**

![](https://i.imgur.com/oKL9ZVD.png)

The `VBox` contains a `Label` and a `ListView`, and the `items` property of the `ListView` is assigned to the `values` property of our `Controller`.

Whether they are reading or writing data, Controllers can have long-running tasks and should not perform work on the JavaFX thread. We will learn how to easily offload work to a worker thread using the `runAsync` construct next.

### Long Running Tasks

Whenever you call a function in a controller, you need to determine if that function returns immediately or if it
performs potentially long-running tasks. If you call a function on the JavaFX Application Thread, the UI
will be unresponsive until the call completes. Unresponsive UI's is a killer for user acceptance, so we need to make sure to run expensive operations in the background. TornadoFX provides the `runAsync` function to help with this.

Code placed inside a `runAsync` block will run in the background. If the result of the background call should update your UI, you must make sure that you apply the changes on the JavaFX Application Thread. The `ui` block, which typically follows, does exactly that.

```kotlin
val textfield = textfield()
button("Update text") {
    action {
        runAsync {
            myController.loadText()
        } ui { loadedText ->
            textfield.text = loadedText
        }
    }
}
```

When the button is clicked, the action inside the `action` builder is run. It makes a call out to `myController.loadText()`and applies the result to the text property of the textfield when it returns. The UI stays responsive while the controller function runs.

Under the covers, `runAsync` creates a JavaFX `Task` objects, and spins off a separate thread to run your call inside the `Task`. You can even assign this `Task` to a variable and bind it to a UI to show progress while your operation is running.

In fact, this is so common that there is also a default ViewModel called `TaskStatus` which contains observable values
 for `running`, `message`, `title`, and `progress`. You can supply the `runAsync` call with a specific instance of the `TaskStatus` object, or use the default. There is also a version of `runAsync` called `runAsyncWithProgress` which will cover the current `Node` with a progress indicator while the long running operation runs.

The TornadoFX sources includes an example usage of this in the `AsyncProgressApp.kt` file.

> If you need to handle a great deal of complex concurrency, you may consider using [RxKotlin](https://github.com/ReactiveX/RxKotlin) with [RxKotlinFX](https://github.com/thomasnield/RxKotlinFX). Rx also has the ability to handle rapid user inputs and events, and kill previous requests to only chase after the latest.

## Fragment

Any `View` you create is a singleton, which means you typically use it in only one place at a time. The reason for this is that the root node of the `View` can only have a single parent in a JavaFX application. If you assign it another parent, it will disappear from its previous parent.

However, if you would like to create a piece of UI that is short-lived or can be used in multiple places, consider using
a `Fragment`. A **Fragment** is a special type of `View` designed to have multiple instances. They are particularly useful for popups or as pieces of a larger UI (such as ListCells, which we look at via the `ListCellFragment` later).

Both `View` and `Fragment` support `openModal()`, `openWindow()`, and `openInternalWindow()` functions that will open the root node in a separate Window.

```kotlin
import javafx.stage.StageStyle
import tornadofx.*


class MyView : View() {
    override val root = vbox {
        button("Press Me") {
            action {
                find<MyFragment>().openModal(stageStyle = StageStyle.UTILITY)
            }
        }
    }
}

class MyFragment: Fragment() {
    override val root = label("This is a popup")
}
```

You can also pass optional arguments to `openModal()` to modify a few of its behaviors

**Optional Arguments for openModal()**

|Argument|Type|Description|
|---|---|---|
|stageStyle|StageStyle|Defines one of the possible enum styles for `Stage`. Default: `StageStyle.DECORATED`|
|modality|Modality|Defines one of the possible enum modality types for `Stage`. Default: `Modality.APPLICATION_MODAL`|
|escapeClosesWindow|Boolean|Sets the `ESC` key to call `close()`. Default: `true`|
|owner|Window|Specify the owner Window for this Stage|
|block|Boolean|Block UI execution until the Window closes. Default: `false`|

## InternalWindow

While `openModal` opens in a new `Stage`, `openInternalWindow` opens over the current root node, or any other node if you specify it:

```kotlin
    button("Open editor") {
        action {
            openInternalWindow<Editor>()
        }
    }
```

**Figure 3.9**

![](https://i.imgur.com/7Z0X5QL.png)

A good use case for the internal window is for single stage environments like JPro, or if you want to customize the window trim to make the window appear more in line with the design of your application. The Internal Window can be styled with CSS. Take a look at the `InternalWindow.Styles` class for more information about styleable properties.

The internal window API differs from modal/window in one important aspect. Since the window opens over an existing node, you typically call `openInternalWindow()` from within the `View` you want it to open on top of. You supply the View you want to show, and you can optionally supply what node to open over via the `owner` parameter.

**Optional Arguments for openInternalWindow()**

|Argument|Type|Description|
|---|---|---|
|view|UIComponent|The component will be the content of the new window|
|view|KClass<UIComponent>|Alternatively, you can supply the class of the view instead of an instance|
|icon|Node|Optional window icon|
|scope|Scope|If you specify the view class, you can also specify the scope used to fetch the view|
|modal|Boolean|Defines if the covering node should be disabled while the internal window is active. Default: `true`|
|escapeClosesWindow|Boolean|Sets the `ESC` key to call `close()`. Default: `true`|
|owner|Node|Specify the owner Node for this window. The window will by default cover the root node of this view.|
|closeButton|Boolean|Whether there is the close button in window. Default: `true`|
|movable|Boolean|Whether the window is movable. Default: `true`|
|overlayPaint|Paint|The paint for overlay part of internal window over window. Default: `c("#000", 0.4)`|

## Closing Modal Windows

Any `Component` opened using `openModal()`, `openWindow()` or `openInternalWindow()` can be closed by calling `close()`. It is also possible to get to the `InternalWindow` instance directly if needed using `findParentOfType(InternalWindow::class)`.

## Replacing Views and Docking Events

With TornadoFX, is easy to swap your current `View` with another `View` using `replaceWith()`, and optionally add a transition. In the example below, a `Button` on each `View` will switch to the other view, which can be `MyView1` or `MyView2` (Figure 3.10).

```kotlin
import tornadofx.*


class MyView1: View() {
    override val root = vbox {
        button("Go to MyView2") {
            action {
                replaceWith<MyView2>()
            }
        }
    }
}

class MyView2: View() {
    override val root = vbox {
        button("Go to MyView1") {
            action {
                replaceWith<MyView1>()
            }
        }
    }
}
```

**Figure 3.10**

![](https://i.imgur.com/IkInGZh.png)


You also have the option to specify a spiffy animation for the transition between the two Views, as shown below.

```kotlin
replaceWith(MyView1::class, ViewTransition.Slide(0.3.seconds, ViewTransition.Direction.LEFT))
```

This works by replacing the `root` `Node` on a given `View` with another `View`'s `root`. There are two functions you can override on `View` to leverage when a View's `root` `Node` is connected to a parent (`onDock()`), and when it is disconnected (`onUndock()`). You can leverage these two events to connect and "clean up" whenever a `View` comes in or falls out. You will notice running the code below that whenever a `View` is swapped, it will undock that previous `View` and dock the new one. You can leverage these two events to manage initialization and disposal tasks.

```kotlin
import tornadofx.*


class MyView1: View() {
    override val root = vbox {
        button("Go to MyView2") {
            action {
                replaceWith<MyView2>()
            }
        }
    }

    override fun onDock() {
        println("Docking MyView1!")
    }

    override fun onUndock() {
        println("Undocking MyView1!")
    }
}

class MyView2: View() {
    override val root = vbox {
        button("Go to MyView1") {
            action {
                replaceWith<MyView1>()
            }
        }
    }

    override fun onDock() {
        println("Docking MyView2!")
    }
    override fun onUndock() {
        println("Undocking MyView2!")
    }
}
```

## Passing Parameters to Views

The best way to pass information between views is often an injected `ViewModel`. Even so, it can still be convenient to be able to pass parameters to other components. The `find` and `inject` functions supports varargs of `Pair<String, Any>` which can be used for just this purpose. Consider a customer list that opens a customer editor for the selected customer. The action to edit a customer might look like this:

```kotlin
fun editCustomer(customer: Customer) {
    find<CustomerEditor>(mapOf(CustomerEditor::customer to customer)).openWindow()
}
```

The parameters are passed as a map, where the key is the property in the view and the value is whatever you want the property to be. This gives you a type safe way of configuring parameters for the target View.

Here we use the Kotlin `to` syntax to create the parameter. This could also have been written as `Pair(CustomerEditor::customer, customer)` if you prefer. The editor can now access the parameter like this:

```kotlin
class CustomerEditor : Fragment() {
    val customer: Customer by param()
}
```

If you want to inspect the parameters instead of blindly relying on them to be available, you can either declare them as nullable or consult the `params` map:

```kotlin
class CustomerEditor : Fragment() {
    init {
        val customer = params["customer"] as? Customer
        if (customer != null) {
            ...
        }
    }
}
```

If you do not care about type safety you can also pass parameters as `mapOf("customer" to customer)`, but then you miss out on automatic refactoring if you rename a property in the target view.

## Accessing the Primary Stage

`View` has a property called `primaryStage` that allows you to manipulate properties of the `Stage` backing it, such as window size. Any `View` or `Fragment` that were opened via `openModal` will also have a `modalStage` property available.

## Accessing the Scene

Sometimes it is necessary to get a hold of the current scene from within a `View` or `Fragment`. This can be achieved with `root.scene`, or if you are within a type safe builder, just call `scene`.

## Accessing Resources

Lots of JavaFX APIs takes resources as a `URL` or the `toExternalForm` of an URL. To retrieve a resource url one would typically write something like:

```kotlin
val myAudioClip = AudioClip(MyView::class.java.getResource("mysound.wav").toExternalForm())
```

However, every `Component` has a `resources` object which can retrieve the external form url of a resource like this:

```kotlin
val myAudiClip = AudioClip(resources["mysound.wav"])
```

If you need an actual `URL`, it can be retrieved like this:

```kotlin
val myResourceURL = resources.url("mysound.wav")
```

The `resources` helper also has several other helpful functions to help you turn files relative to the `Component` into an object of the type you need:

```kotlin
val myJsonObject = resources.json("myobject.json")
val myJsonArray = resources.jsonArray("myarray.json")
val myStream = resources.stream("somefile")
```

>It is worth mentioning that the `json` and `jsonArray` functions are also available on `InputStream` objects.

Resources are relative to the `Component` but you can also retrieve a resource by it's full path, starting with a `/`.


## Summary

TornadoFX is filled with simple, streamlined, and powerful injection tools to manage Views and Controllers. It also streamlines dialogs and other small UI pieces using `Fragment`. While the applications we built so far are pretty simple, hopefully you appreciate the simplified concepts TornadoFX introduces to JavaFX. In the next chapter we will cover what is arguably the most powerful feature of TornadoFX: Type-Safe Builders.
