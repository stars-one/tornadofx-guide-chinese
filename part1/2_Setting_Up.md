# 开始

你可以使用两种构建方式来使用TornadoFx，[Gradle](http://gradle.org/)和 [Maven](https://maven.apache.org/)

需要注意的是，TornadoFx是一个Kotlin的库，因此你的项目必须配置好Kotlin的环境。详细配置请查阅[Kotlin Gradle 安装](https://kotlinlang.org/docs/reference/using-gradle.html) 和 [Kotlin Maven 安装](https://kotlinlang.org/docs/reference/using-maven.html) 。同时，确保你的开发环境或开发IDE部署了kotlin的插件或编译器。

此说明将使用Intellij IDEA进行示例的开发及演示。IDEA是Kotlin语言开发的一种选择，你也可以使用Eclipse，不过需要额外的进行插件的配置。

## 如果你使用Oracle JDK

### Gradle
关于Gradle，你可以从Maven中央仓库中直接添加TornadoFx的依赖，只需要提供相应的版本即可直接依赖Maven中央仓库中的依赖，如下所示：

```
repositories {
    mavenCentral()
}
//最低需要1.8版本的jvmTarget
compileKotlin {
    kotlinOptions.jvmTarget= 1.8
}

dependencies {
    implementation 'no.tornado:tornadofx:x.y.z'
}
```

### Maven
使用Maven导入TornadoFx，你需要将下列依赖添加到你的POM文件中

使用kotlin-maven-plugin代码块,首先需要添加配置文件。
```
<configuration>
    <jvmTarget>1.8</jvmTarget>
</configuration>
```
之后添加TornadoFx依赖：

```
<dependency>
    <groupId>no.tornado</groupId>
    <artifactId>tornadofx</artifactId>
    <version>x.y.z</version>
</dependency>
```

## 如果你使用OpenJDK

在Ubuntu 19.10上，不再有使用JFX运行OpenJDK 8的干净方法。 OpenJDK通常不包括JFX模块库-OpenJFX通常为OpenJDK提供JFX支持，它具有maven发行版以及各种Linux发行版中的软件包。 

但是，OpenJFX版本与JDK版本相关（例如，OpenJFX 8与OpenJDK 8兼容），不幸的是，OpenJFX 8版本在Ubuntu 19.10的软件包中不可用，也无法使用打包的OpenJDK 8从源代码进行编译。 这样做的结果是，如果您希望继续使用OpenJDK（在Ubuntu上），则可以选择：

1）保持OpenJDK 8的状态，但是通过向系统中添加较旧的依赖项来安装OpenJFX 8系统范围（例如[https://bugs.launchpad.net/ubuntu/+source/openjfx/+bug/1799946/comments/7](https://bugs.launchpad.net/ubuntu/+source/openjfx/+bug/1799946/comments/7)）

2）另外，您也可以硬着头皮升级到OpenJDK 11，然后通过Maven / Gradle安装OpenJFX。 该解决方案如下：

### Gradle

a) 通过系统的打包工具升级到OpenJDK 11

b) 添加OpenJFX gradle插件:

```
plugins {
    id 'application'
    id 'org.openjfx.javafxplugin' version '0.0.8'
}
```

c) 添加gradle

```
javafx {
    version = "11.0.2"
    modules = ['javafx.controls', 'javafx.graphics']
}

dependencies {
    implementation 'no.tornado:tornadofx:x.y.z'
}
```

d) 修改 kotlin `jvmTarget`为 `11`

```
compileKotlin {
    kotlinOptions.jvmTarget = "11"
}

compileTestKotlin {
    kotlinOptions.jvmTarget = "11"
}
```

e) 由于gradle不能正确支持 `JPMS`,所以我们的项目不需要添加`module-info.java` 

在IDEA中,你需要将`Project SDK`改为11,具体流程为`File -> Project Structure -> Project Settings -> Project`. 

### Maven

a) 通过系统的打包工具升级到OpenJDK 11

b) 添加maven依赖
```
        <dependency>
            <groupId>no.tornado</groupId>
            <artifactId>tornadofx</artifactId>
    	    <version>x.y.z</version>
        </dependency>

        <dependency>
            <groupId>org.openjfx</groupId>
            <artifactId>javafx</artifactId>
            <version>11.0.2</version>
        </dependency>
        <dependency>
            <groupId>org.openjfx</groupId>
            <artifactId>javafx-base</artifactId>
            <version>11.0.2</version>
        </dependency>
        <dependency>
            <groupId>org.openjfx</groupId>
            <artifactId>javafx-controls</artifactId>
            <version>11.0.2</version>
        </dependency>
```

c) 在maven的构建插件添加OpenJFX builder
```
<build>
   <plugins>
       ...
	<plugin>
            <groupId>org.openjfx</groupId>
            <artifactId>javafx-maven-plugin</artifactId>
            <version>0.0.3</version>
            <configuration>
                <mainClass>MyMainApp</mainClass>
            </configuration>
        </plugin>
   </plugins>
</build>
```

d) 设置你当前的maven的语言等级
```
<build>
   <plugins>
	...
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-compiler-plugin</artifactId>
            <version>3.8.0</version>
            <configuration>
                <release>11</release>
            </configuration>
        </plugin>
    </plugins>
</plugin>
```

e) 最后,添加`src/main/kotlin/module-info.java`,在java11的module系统中设置声明许可

```
module yourmodulename {
    requires javafx.controls;
    requires javafx.graphics;
    requires tornadofx;
    requires kotlin.stdlib;
    opens your.package.to.ui.classes;
}
```
如果你是使用的IDEA，它将提供有用的提示添加任何额外的模块,您可能需要与你的应用程序构建成功。StackOverflow过多的问题从Java程序员切换模块系统第一次,所以,如果你遇到一个模块权限相关的问题，请通过谷歌引擎进行搜索关于该具体错误消息的解决方案。

## 其他构建自动化解决方案

关于如何将tornado ofx与其他构建自动化解决方案一起使用的说明，请跳转[TornadoFX page at the Central Repository](https://search.maven.org/artifact/no.tornado/tornadofx/)

## 手动导入

手动下载TornadoFx的jar包文件（具体文件请访问[TornadoFX release page](https://github.com/edvin/tornadofx/releases)，在项目中导入并配置。
